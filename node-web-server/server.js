const express = require('express');
const hbs = require('hbs'); // this lib allows you to create hbs files (html files where you can use {{ }} to print vars as Angular does)
const fs = require('fs');

// Heroku port or 3000 port instead
const port = process.env.PORT || 3000;
var app = express();

hbs.registerPartials(__dirname + '/views/partials'); // Parts of html code inside of another hbs files (like components to reuse)
// nodemon server.js -e js,hbs  (to run in partial mode)

app.set('view engine', 'hbs');

// middleware
app.use((req, res, next) => {
  var now = new Date().toString();
  var log = `${now}: ${req.method} ${req.url}`;

  console.log(log);
  // appending the log in a file called 'server.log'
  fs.appendFile('server.log', log + '\n', (err) => {
    if (err) {
      console.log('Unable to append to server.log.');
    }
  });
  next(); // next() will allow the app to continue and loading everything, whithout it, it will keep loading for ever
});

// this middleware is going to stop all the next executions to come, due to we are not calling next()
// instead, it will show the maintenance.hbs page with the render() function that we are using from the response object
/*UPDATING SITE middleware (uncomment to stop the app of executing and show maintenance.hbs page instead)*/
// app.use((req, res, next) => {
//   res.render('maintenance.hbs', {
//     infoMessage: {
//       title: 'We will be right back.',
//       description: 'The site is currently being updated.'
//     }
//   });
// });


// __dirname = /<file_name_html> + /public prepended to find it (localhost:3000/help.html)
// we put this here to don't allow the help.html file be show in the url if the middleware maintenance.hbs is executed.
app.use(express.static(__dirname + '/public'));

// helper is a var that you can use globally for the same function without passing it as attribute all the time in each page
hbs.registerHelper('getCurrentYear', () => {
   return new Date().getFullYear();
});

// help who takes params {{ screamIt <param_name> }}
hbs.registerHelper('screamIt', (text) => {
  return text.toUpperCase()
});

// router
app.get('/', (req, res) => {
  // render() is to send html/hbs files with params (vars)
  res.render('home.hbs', {
    pageTitle: 'Home Page',
    welcomeMessage: 'Welcome to the Home Page'
  })
});

app.get('/about', (req, res) => {
  // Here I a passing a html(mustache) page (hbs)
  // The second object are the variables that I can print in the file using the handlebars(mustache) {{ }}
  // This is how Angular does it!
  res.render('about.hbs', {
    pageTitle: 'About Page',
  });
});

app.get('/projects', (req, res) => {
  res.render('projects.hbs', {
    pageTitle: 'Projects Page',
    description: 'Your projects will be listed in here.'
  });
});

// /bad - send back json with errorMessage property
app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'The page were not found.'
  });
});

// port to listen
app.listen(port, () => {
  console.log(`Server is up in port ${port}`);
});
