const express = require('express');

var app = express();

app.get('/', (req, res) => {
  // sendind two different things (status and send())
  res.status(404).send({
    error: 'Page not found.',
    name: 'ToDo App v1.0'
  });
});

// GET /users => array of users
// Give users a name property and age property
app.get('/users', (req, res) => {
  res.status(202).send([
    { name: 'Abel', age: 25 },
    { name: 'Maryna', age: 21},
    { name: 'Isabel', age: 35 }
  ]);
});

app.listen(3000, () => {
  console.log('Server is running in port 3000.');
});

module.exports.app = app;
