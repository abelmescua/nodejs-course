// to test express apps
const request = require('supertest');
const expect = require('expect');

// the app of 'server.js' with all the functions of it to test them
var app = require('./server').app;

// Server
describe('Server', () => {
  /**
    SUPERTEST for express apps using ('supertest' and 'expect' libraries together)
  **/

  // GET /
  describe('GET /', () => {
    // some test case
    it('should return page not found object response', (done) => {
      request(app)
        .get('/')
        // expecting two different responses
        .expect(404)
        // combination of supertest and expect
        .expect((res) => {
          expect(res.body).toInclude({
            error: 'Page not found.'
          });
        })
        .end(done);
    });
  });

  // GET /users
  describe('GET /users', () => {
    // some test case
    // Make a new test
    // assert 200
    // Assert that you exist in users array
    it('should return an array of users', (done) => {
      request(app)
        .get('/users')
        // we must get 202 response
        .expect(202)
        // also we must get a res with the next requirements
        .expect((res) => {
          expect(res.body).toInclude({
            name: 'Abel',
            age: 25
          });
        })
        .end(done);
    });
  });
});
