// .test.js means that this file is a test case

// the file that we are going to test
const utils = require('./utils');

// assertions library to make our test cases statements better
const expect = require('expect');

// describe makes our test more readable. Inside of it I can put the test of the 'utils.js.test' file and name them as 'Utils'
describe('Utils', () => {

  // inside of it also I can separate test for specific functions
  describe('#add', () => {
    // 'it()' is a function provided by mocha
    it('should add two numbers', () => {
      var res = utils.add(33, 11); // it should return 44

      // Error detector with expect() (same than below, but in one single line)
      expect(res)
      // expected value
      .toBe(44)
      // expected type
      .toBeA('number');
    });

    // when we have (done) it means that the test won't be finish util done(); is executed inside of it
    // this is normally used when you are testing assincrony functions that returns after a while a callback
    it('should async add two numbers', (done) => {
      utils.asyncAdd(4, 3, (sum) => {
        expect(sum).toBe(7).toBeA('number');
        done();
      });
    });
  });

  // square tests
  describe('#square', () => {
    // to test square() function
    it('should return the square of a number' , () => {
      var res = utils.square(3); // It should return 4

      // Error handeling with assertions of 'expect' library
      expect(res).toBe(9).toBeA('number');
    });

    // test asyncSquare()
    it('should async return the square of a number', (done) => {
      utils.asyncSquare(3, (res) => {
        expect(res).toBe(9).toBeA('number');
        done();
      });
    });
  });

  // users operations
  describe('#users', () => {
    // should veriry first and last names are set
    // send user object with name and location seted
    // also you will pass an string with your first and last name separated by an space
    // assert it includes firstName and lastName with proper given values
    it('should return an user object with firstName and lastName seted according to the given name', () => {
      var user = { age: 25, location: 'Campanillas' }; // user object where to set the name
      var res = utils.setName(user, 'Abel Mescua'); // calling the function and returning the result of the user object

      // test case with assertions, the response should include firstName and lastName with the current values assigned
      expect(res).toInclude({ firstName: 'Abel', lastName: 'Mescua'});
    });
  });
});
// describe end#

it('should expect some values', () => {
  // expect(12).toNotBe(12);
  // toBe() does not work with objects
  // expect({name: 'Abel'}).toBe({name: 'Abel'});

  // To compare objects we user toEqual()
  expect({name: 'Abel'}).toEqual({name: 'Abel'});

  // to check if an object or array included an especific value
  // expect([2,3,4]).toInclude(5);
  // to check if a value is not in an array or an object
  // expect([2,3,4]).toExclude(5);

  // Example with an object to se if it has a value
  expect({
    name: 'Abel',
    age: 25,
    location: 'Malaga'
  }).toExclude({
    age: 23
  })
});

// mocha **/*.test.js  (this command will execute all test files in mocha)
// nodemon --exec 'npm test'  (will run 'npm test' in nodemon mode, so the test are runn each time that we refresh your app alive)
