// SPIES
const expect = require('expect');
const rewire = require('rewire');

// rewire loads your files through require, but also add two methods
var app = rewire('./app');
// app.__set__
// app.__get__


// describing the methods of the app.js tests
describe('App', () => {
  // setting the db spy for db.js
  var db = {
    saveUser: expect.createSpy() // creating an spy
  };
  app.__set__('db', db); // replacing original db.js for the spy db with the same functions to call as spies

  it('should call the spy correctly', () => {
    var spy = expect.createSpy(); // creating the spy
    spy('Andrew', 25); // calling the spy
    // checking if the spy was called or not
    expect(spy).toHaveBeenCalledWith('Andrew', 25);
  });

  // using db spy in here
  it('should call saveUser with user object', () => {
    var email = 'abel@example.com';
    var password = '123abc';

    app.handleSignup(email, password); // this function will call db.js, but instead it will use our db var with the spy
    expect(db.saveUser).toHaveBeenCalledWith({ email, password }); // here we check if our spy was called or not
  });
});
