const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost')
    .then(() => console.log('Connected to MongoDB...'))
    .catch((err) => console.log('Could not connect to MongoDB...', err.message));

const couserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255
    },
    category: {
        type: String,
        required: true,
        enum: ['web', 'network', 'mobile'],
        lowercase: true,
        trim: true
    },
    author: String,
    tags: {
        type: Array,
        validate: {
            isAsync: true,
            validator: function(v, callback) {
                setTimeout(() => {
                    // Do some async work
                    const result = v && v.length > 0;
                    callback(result);
                }, 4000);
                return v && v.length > 0;
            },
            message: 'A course should have at least one tag.'
        }
    },
    date: { type: Date, default: Date.now },
    isPublished: Boolean,
    price: {
        type: Number,
        required: function () { return this.isPublished; },
        min: 10,
        max: 200,
        get: v => Math.round(v),
        set: v => Math.round(v)

    }
});

const Course = mongoose.model('Course', couserSchema);

async function createCourse() {
    const course = new Course({
        name: 'Nodejs Course',
        category: 'Web',
        author: 'Mosh',
        tags: ['frontend'],
        isPublished: true,
        price: 15.8
    });

    try {
        const result = await course.save();
        console.log(result);
    } catch (ex) {
        for (field in ex.errors) {
            console.log(ex.errors[field].message);
        }
    }
}

async function getCoursesWithFilters() {
    // Filter query operators
    // eq (equal)
    // ne (not equal)
    // gt (greater than)
    // gte (greater than o equal to)
    // lt (less than)
    // lte (less than or equal to)
    // in
    // nin (not in)
    // Logical Query operators
    // or
    // and

    const courses = await Course
        // .find({ author: 'Mosh', isPublished: true })
        // .find({ price: { $gte: 10, $lte: 20 } })
        // .find({ price: { $in: [10, 15, 20]} })
        // .find()
        // .or([ { author: 'Mosh' }, { isPublished: true } ])
        // Starts with Mosh
        .find({ author: /^Mosh/ })
        // Ends with Hamedani
        .find({ author: /Hamedani$/i })
        // Contains Mosh
        .find({ author: /.*Mosh.*/ })
        .limit(10)
        .sort({ name: 1 })
        .count();
    // .select({ name: 1, tags: 1 });
    console.log('Courses', courses);
}

async function getCourses() {
    const pageNumber = 2;
    const pageSize = 10;
    // /api/courses?pageNumber=2&pageSize=10

    const courses = await Course
        .find({ _id: '5ca62aa24921fc0364e62393' })
        // .skip((pageNumber - 1) * pageSize)
        // .limit(10)
        .sort({ name: 1 })
        .select({ price: 1 });
    console.log(courses);
}

async function updateCourseQueryFirst(id) {
    const course = await Course.findById(id);
    if (!course) return;

    course.isPublished = true;
    course.author = 'Another Author';

    const result = await course.save();
    console.log('result', result);
}

async function updateCourseWithUpdateDirectly(id) {
    const result = await Course.update({ _id: id }, {
        $set: {
            author: 'Mosh',
            isPublished: false
        }
    });
    console.log('result', result);
}

async function updateCourse(id) {
    const course = await Course.findOneAndUpdate(id, {
        $set: {
            author: 'Jason',
            isPublished: false
        }
    }, { new: true }); // to return the updated object instead the old one by default
    console.log('result', course);
}

async function removeCourse(id) {
    // const result = await Course.deleteOne({ _id: id });
    const result = await Course.deleteMany({ _id: id });
    const course = await Course.findByIdAndDelete(id);
    console.log(course);
}
// createCourse();
getCourses();