const { ObjectID } = require('mongodb');

const { mongoose } = require('./../server/db/mongoose');
const { Todo } = require('./../server/models/todo');
const { User } = require('./../server/models/user');

// Remove all records
// Todo.remove({}).then((res) => {
//   console.log(res);
// });

// Todo.findOneAndRemove({_id: new ObjectID('5bc1c3cf0740094686ff1442')}).then((todo) => {
//   console.log(todo);
// });
//
// Todo.findByIdAndRemove('5bc1c3150740094686ff1421').then((todo) => {
//   console.log(todo);
// });
