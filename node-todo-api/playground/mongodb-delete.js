
// taking out a propery from an object and putting it into a var with the same name
const { MongoClient, ObjectID } = require('mongodb');

// connecting to database
MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to the MongoDB server.');
  }
  console.log('Connected to MongoDB server.');

  // deleteMany with the {text: 'Eat lunch'} property
  // db.collection('Todos').deleteMany({text: 'Eat lunch'}).then((result) => {
  //   console.log(result);
  // });

  // deleteOne only with the {text: 'Eat lunch'} property
  // db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((result) => {
  //   console.log(result);
  // });

  // (RECOMENDED)
  // findOneAndDelete it will delete only one with {completed: false} property and it will show the item to you also in the console with all its data
  // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
  //   console.log(result);
  // });

  // Challenge
  // delete all similar
  // db.collection('Users').deleteMany({name: 'Max'}).then((result) => {
  //   console.log(result);
  // }, err => {
  //   console.log('Error: ', err);
  // });

  // delete one by _id
  // db.collection('Users').findOneAndDelete({_id: new ObjectID('5bae5a6d8d15396dae6cf98b')}).then((result) => {
  //   console.log(JSON.stringify(result, undefined, 2));
  // }, err => {
  //   console.log('Error: ', err);
  // });

  // disconecting from the database
  // db.close();
});
