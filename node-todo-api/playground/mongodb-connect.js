// const MongoClient = require('mongodb').MongoClient;

// var user = { name: 'Abel', age: 25 };
// var {name} = user; // taking out a propery from an object and putting it into a var with the same name
// console.log(name);

// taking out a propery from an object and putting it into a var with the same name
const { MongoClient, ObjectID } = require('mongodb');

// var obj = new ObjectID();
// console.log(obj);

// connecting to database
MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to the MongoDB server.');
  }
  console.log('Connected to MongoDB server.');

  // Makind insertion operation into database
  // db.collection('Todos').insertOne({
  //   text: 'Something to do',
  //   completed: false
  // }, (err, result) => {
  //   if (err) {
  //     return console.log('Unable to insert todo', err);
  //   }
  //
  //   console.log(JSON.stringify(result.ops, undefined, 2));
  // });

  // Insert new doc into Users(name, age, location)
  // db.collection('Users').insertOne({
  //   name: 'Abel',
  //   age: 25,
  //   location: 'Campanillas'
  // }, (err, result) => {
  //   if (err) {
  //     return console.log('Unable to insert user', err);
  //   }
  //
  //   // To know the time that the object was created
  //   console.log(result.ops[0]._id.getTimestamp());
  // });

  // disconecting from the database
  db.close();
});
