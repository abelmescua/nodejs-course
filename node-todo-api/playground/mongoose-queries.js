const { ObjectID } = require('mongodb');

const { mongoose } = require('./../server/db/mongoose');
const { Todo } = require('./../server/models/todo');
const { User } = require('./../server/models/user');

// var id = '5bb84a85c2aa9532046c42bd';
var id = '5bb259825b1bdc5d0495a057';

if (!ObjectID.isValid(id)) {
  console.log('ID not valid');
}

// Todo.find({
//   _id: id
// }).then((todos) => {
//   console.log('Todos', todos);
// });
//
// Todo.findOne({
//   _id: id
// }).then((todo) => {
//   console.log('Todo', todo);
// });
//
// Todo.findById(id).then((todo) => {
//   if (!todo) {
//     return console.log('Id not found');
//   }
//   console.log('Todo By Id', todo);
// }).catch((e) => console.log(e));


// User.findById
User.findById(id).then((todo) => {
  if (!todo) {
    return console.log('Id not found');
  }
  console.log('Todo By Id', todo);
}).catch((e) => console.log(e));

// User.findOne
User.findOne({
  _id: id
}).then((todo) => {
  console.log('Todo', todo);
}, (e) => {
  console.log(e);
});

// User.find
User.find({
  _id: id
}).then((todo) => {
  console.log('Todos', todos);
}, (e) => {
  console.log(e);
});
