
// taking out a propery from an object and putting it into a var with the same name
const { MongoClient, ObjectID } = require('mongodb');

// connecting to database
MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to the MongoDB server.');
  }
  console.log('Connected to MongoDB server.');

  // findOneAndUpdate
  // db.collection('Todos').findOneAndUpdate({
  //   _id: new ObjectID('5bae552f8d15396dae6cf76c')
  // }, {
  //   $set: {
  //     completed: true
  //   }
  // }, {
  //   returnOriginal: false // to return the value with the attribute changed
  // }).then((result) => {
  //   console.log(JSON.stringify(result, undefined, 2));
  // });

  // increment operator
  db.collection('Users').findOneAndUpdate({
    _id: new ObjectID('5babcda513270304c8ddc01d')
  }, {
    // increment operator
    $inc: {
      age: 15
    },
    // setting name to that value
    $set: {
      name: 'Abel'
    }
  }, {
    returnOriginal: false
  }).then((result) => {
    console.log(JSON.stringify(result, undefined, 1));
  });

  // disconecting from the database
  // db.close();
});
