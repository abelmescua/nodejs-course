const { SHA256 } = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

var password = '123abc!';

// (number_for_randoms, callback)
// bcrypt.genSalt(10, (err, salt) => {
//     bcrypt.hash(password, salt, (err, hash) => {
//         console.log(hash);
//     });
// });

var hashedPassword = '$2a$10$rw7NGYRl4vu/lQHPgmPqDOuMQTGQh3wtNFlZvmlgN4mW2KQcwJpnS';

// To know if a password and its hashed value are the same (check that it wasn't change by someone else)
bcrypt.compare('password', hashedPassword, (err, res) => {
    console.log(res);
});

// var data = {
//     id: 10
// };

// // encoding with jwt
// var token = jwt.sign(data, '123abc');
// console.log(token);

// // decoding with jwt
// var decoded = jwt.verify(token, '123abc');
// console.log('decoded', decoded);


// Encripting a message
// var message = 'I am user number 3';
// var hash = SHA256(message).toString();

// console.log(`Message: ${message}`);
// console.log(`Hash: ${hash}`);

// var data = {
//     id: 4
// };

// var token = {
//     data,
//     // encripted data
//     hash: SHA256(JSON.stringify(data) + 'somesecret').toString()
// };

// token.data.id = 5;
// token.hash = SHA256(JSON.stringify(token.data)).toString();

// // JSON WEB TOKEN example (data structure to send auth in the web)

// // enciprted data with some changes (hacking case)
// var resultHash = SHA256(JSON.stringify(token.data) + 'somesecret').toString();
// // checking encription in a secure way
// if (resultHash === token.hash) {
//     console.log('Data was not changed');
// } else {
//     console.log('Data was changed. Do not trust!');
// }