
// taking out a propery from an object and putting it into a var with the same name
const { MongoClient, ObjectID } = require('mongodb');

// connecting to database
MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to the MongoDB server.');
  }
  console.log('Connected to MongoDB server.');

  // to get all the items from the Todos collection db > find()
  // to the those items whose attribute 'completed' is 'false' > find({completed: false})
  // to fint an item by its _id attribute, that is save as an objectID in the mongodb > find({_id: new ObjectID('5babcca2e3ecbe04a9c51600')})
  // db.collection('Todos').find({_id: new ObjectID('5babcca2e3ecbe04a9c51600')}).toArray().then((docs) => {
  //   console.log('Todos');
  //   console.log(JSON.stringify(docs, undefined, 2));
  // }, (err) => {
  //   console.log('Unable to fetch todos', err);
  // });

  // take the amount of tiems that we have in Todos collection
  // db.collection('Todos').find().count().then((count) => {
  //   console.log('Todos count:', count);
  // }, (err) => {
  //   console.log('Unable to fetch todos', err);
  // });

  // finding an user by name
  db.collection('Users').find({name: 'Jen'}).toArray().then((docs) => {
    // the other to params is to show it in different lives as a normal json, instead of one line everything
    console.log(JSON.stringify(docs, undefined, 2));
  }, err => {
    console.log('Error: ', err);
  });

  // disconecting from the database
  db.close();
});
