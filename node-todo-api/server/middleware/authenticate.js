const { User } = require('./../models/user');

/**
 * This method will check if a user is logged in or not, to allow it to access to the app routes or not
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
var authenticate = (req, res, next) => {
    var token = req.header('x-auth');

    User.findByToken(token).then((user) => {
        if (!user) {
        return Promise.reject();
        }
        console.log('Success', user);
        
        req.user = user;
        req.token = token;
    }).catch((e) => {
        console.log('Error', e);
        res.status(401).send();
    });
};

module.exports.authenticate = authenticate;