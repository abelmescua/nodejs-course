// using mongoose to make the db operatinos as we did with mongodb in playground, but with less coding!!
var mongoose = require('mongoose');

// Using mogoose to connect with the db that we want
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI);

module.exports = {
  mongoose: mongoose
};

process.env.NODE_ENV = 'production';