const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

// creating a Schema in the User model to allow us add new functions on it
var UserSchema = new mongoose.Schema({
  // email
  email: {
    type: String,
    required: true,
    minlength: 5,
    trim: true,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email'
    }
  },
  // password
  password: {
    type: String,
    require: true,
    minlength: 6
  },
  // access token
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
});

UserSchema.methods.toJSON = function () {
  var user = this;
  var userObject = user.toObject();

  return _.pick(userObject, ['_id', 'email']);
};

// adding new method to User shcema called 'generateAuthToken'
UserSchema.methods.generateAuthToken = function () {
  var user = this;
  var access = 'auth';
  var token = jwt.sign({ _id: user._id.toHexString(), access}, process.env.JWT_SECRET).toString();

  user.tokens = user.tokens.concat([{access, token}]);

  return user.save().then(() => {
    return token;
  });
};

UserSchema.statics.findByToken = function(token) {
  var User = this;
  var decoded;
  
  try {
    console.log('verifying token', token);
    jwt.verify(token, process.env.JWT_SECRET);
    console.log('token verified');
    
  } catch (e) {
    console.log('Token is not trusty');
    
    return Promise.reject();
  }
  console.log(decoded._id);
  
  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });
};

UserSchema.methods.removeToken = function (token) {
 var user = this;

 return user.update({
   $pull: {
     tokens: {
       token: {token}
     }
   }
 });
};

UserSchema.statics.findByCredentials = function (email, password) {
  var User = this;
  return User.findOne({email}).then((user) => {
    // If user does not exist
    if (!user) {
      return Promise.reject();
    }

    // If user exist
    return new Promise((resolve, reject) => {
      // User bcrypt.compare to compare password and user.password
      bcrypt.compare(user.password, password, (err, res) => {
        if (res) {
          resolve(user);
        } else {
          reject();
        }
      });
    });
  });
};

// To do something before the save() function is called
// this is a middlware, we are using it to script a password
UserSchema.pre('save', function (next) {
  var user = this;

  if (user.isModified('password')) {
    // user.password
    bcrypt.genSalt(10, (err, salt) => {
      // salt went well?
      if (salt) {
        bcrypt.hash(user.password, salt, (err, hash) => {
          // hash went well?
          if (hash) {
            bcrypt.compare(user.password, hash, (err, res) => {
              // compare is correct?
              if (res) {
                user.password = hash;
              } else {
                console.log('Something went wrong hashing password', err);
              }
              next();
            });
          } else {
            next();
          }
        });
      } else {
        next();
      }
    });
    // user.password = hash;
    // next()
  } else {
    next();
  }
});

var User = mongoose.model('Users', UserSchema);

module.exports.User = User;
