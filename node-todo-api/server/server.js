require('./config/config');

const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const { ObjectID } = require('mongodb');


// Database connection
var { mongoose } = require('./db/mongoose');

// Models
var { Todo } = require('./models/todo');
var { User } = require('./models/user');
const { authenticate } = require('./middleware/authenticate');

var app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.post('/todos', authenticate, (req, res) => {
  var todo = new Todo({
    text: req.body.text,
    _creator: req.user._id
  });

  todo.save().then((doc) => {
    // what does it does?
    res.send(doc);
  }, (e) => {
    res.status(400).send(e);
  });
});

// GET
app.get('/todos', authenticate, (req, res) => {
  Todo.find({
    _creator: req.user._id
  }).then((todos) => {
    res.send({todos});
  }, (e) => {
    res.status(400).send(e);
  });
});


// GET /todos/:id
app.get('/todos/:id', authenticate, (req, res) => {
  var id = req.params.id;
  
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }
  
  Todo.findOne({
    _id: id,
    _creator: req.user._id
  }).then((todo) => {
    // Not found case
    if (!todo) {
      return res.status(404).send();
    }
    
    // Success case
    res.send({todo});
    // Erro case
  }).catch((e) => {
    res.status(400).send();
  });
});

app.delete('todos/:id', (req, res) => {
  // get the id
  var id = req.params.id;
  // validate the id -> not valid? return 404
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }
  // remove todo by id
  Todo.findByIdAndRemove({
    _id: id,
    _creator: req.user._id
  })
  // Success
  .then((todo) => {
    // if no doc, send 404
    if (!todo) {
      return res.status(404).send();
    }
    // if doc, send doc back with 200
    res.status(200).send({todo});
  })
  // error
  .catch((e) => {
    // 400 with empty body
    return res.status(404).send();
  });
});

app.patch('/todos/:id', authenticate, (req, res) => {
  var id = req.params.id;
  // picking up two specific values from the body array
  var body = _.pick(req.body, ['text', 'completed']);
  
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }
  
  if (_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime();
  } else {
    body.completed = false;
    body.completedAt = null;
  }
  
  Todo.findByOneAndUpdate({_id: id, _creator: req.user._id}, {$set: body}, {new: true}).then((todo) => {
    if (!todo) {
      return res.status(404).send();
    }
    
    res.send({todo});
  }).catch((e) => {
    res.status(400).send();
  });
});

// GET /users
app.get('/users', (req, res) => {
  User.find().then((users) => {
    res.send({users});
  }).catch((e) => {
    res.status(404).send({e});
  });
});

app.get('/users/:id', (req, res) => {
  var id = req.params.id;

  if (ObjectID.isValid(id)) {
    return res.status(400).send('Not valid Id');
  }

  User.findById({id})
  .then((user) => {
    if (!user) {
      res.status(400).send('Not found');
    }

    res.send({user});
  }).catch((e) => {
    res.status(404).send({e});
  })
});

// POST /users
app.post('/users', (req, res) => {
  // user _.pick to take the values (email, password) into a var
  var body = _.pick(req.body, ['email', 'password']);
  // body = { email: 'email@test.com', password: 'password123' };
  var user = new User(body); 
  
  // first case of success, we take the user and generate an auth token for it
  user.save().then(() => {
    return user.generateAuthToken();
  // in case of success, we send the user object to the requester
  }).then((token) => {
    res.header('x-auth', token).send(user);
  // in case of failure, we send the error object to the requester with a status 404 included on it
  }).catch((e) => {
    res.status(404).send(e);
  });
});



app.get('/users/me', authenticate, (req, res) => {
  res.send(req.user);
});

// POST /users/login {email, password}
app.post('/users/login', (req, res) => {
  var body = _.pick(req.body, ['email', 'password']);

  User.findByCredentials(body.email, body.password).then((user) => {
    return user.generateAuthToken().then((token) => {
      res.header('x-auth', token).send(user);
    });
  }).catch((e) => {
    res.status(400).send();
  });
});

// DELETE
app.delete('/users/me/token', authenticate, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    // if it goes well, we send positive params
    res.status(200).send();
  }, () => {
    // if it goes bad, we send negative params
    res.status(400);
  });
});

app.listen(port, () => {
  console.log('Started on port 3000');
});

module.exports = { app };
