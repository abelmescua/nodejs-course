const FizzBuzz = require('../exercise1');

describe('FizzBuzz', () => {
    it('should throw exception when input is not a number', () => {
        const args = ['1', null, undefined, {}];
        args.forEach(a => {
            expect(() => { FizzBuzz.fizzBuzz(a) }).toThrow();
        });
    });

    it('should return "FizzBuzz" if input is divisible by 3 and 5', () => {
        const result = FizzBuzz.fizzBuzz(15);
        expect(result).toBe('FizzBuzz');
    });

    it('should return "Fizz" when the input is divisible by 3 only', () => {
        const result = FizzBuzz.fizzBuzz(3);
        expect(result).toBe('Fizz');
    });

    it('should return "Buzz" when the input is divisible by 5 only', () => {
        const result = FizzBuzz.fizzBuzz(5);
        expect(result).toBe('Buzz');
    });

    it('should return the input result if input is a number and not divisible by 3 either by 5', () => {
        const result = FizzBuzz.fizzBuzz(2);
        expect(result).toBe(2);
    });
});