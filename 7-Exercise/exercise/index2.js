const mongoose = require('mongoose');

async function initDB() {
    try {
        const connection = await mongoose.connect('mongodb://localhost/mongo-exercises');
        console.log('Connected to MongoDB...');
    } catch (err) {
        console.log('Error connecting to MongoDB...');
    }
}

initDB();

// Schema
const courseSchema = mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    date: { type: Date, default: Date.now },
    isPublished: Boolean
});

// Model
const Course = mongoose.model('Course', courseSchema);

async function getCourses() {
    return await Course
        .find({ isPublished: true })
        .or([
            { price: { $gte: 15 } },
            { name: /.*by.*/i }
        ])
        .sort({ price: -1 })
        .select({ name: 1, author: 1, price: 1 });
}

async function run() {
    const courses = await getCourses();
    console.log(courses);
}

run();