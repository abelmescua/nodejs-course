const mongoose = require('mongoose');

async function initDB() {
    try {
        const connection  = await mongoose.connect('mongodb://localhost/mongo-exercises');
        console.log('Connected to MongoDB...');
    } catch (err) {
        console.log('Error Connecting MongoDB...', err.message);
    }
}

initDB();

const courseSchema = mongoose.Schema({
    name: String,
    author: String,
    tags: [ String ],
    date: { type: Date, default: Date.now },
    isPublished: Boolean
});

const Course = mongoose.model('Course', courseSchema);

async function getCourses() {
    return await Course
        .find({ isPublished: true, tags: 'backend' })
        .sort({ name: 1 })
        .select({ name: 1 , tags: 1});
}

async function run() {
    const courses = await getCourses();
    console.log(courses);
}

run();