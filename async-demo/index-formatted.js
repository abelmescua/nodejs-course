console.log('Before');

const user = getUser(1, displayUser);

console.log('After');

function displayUser(user) {
    getRepositories(user.gitHubUserName, getCommits)
}

function getCommits(repos) {
    getCommits(repos[0], displayCommits);
}

function displayCommits(commits) {
    console.log('commits here...');
}

function getUser(id, callback) {
    setTimeout(() => {
        console.log('Reading a user from a database...');
        callback({ id: id, gitHubUserName: 'amescua' });
    }, 2000);
}

function getRepositories(username, callback) {
    setTimeout(() => {
        console.log('Reading repositores from the user...');
        callback(['repo1', 'repo2', 'repo3']);
    }, 2000);
}

function getCommits(repo, callback) {
    setTimeout(() => {
        console.log('Reading the commits from the repo...');
        callback(['repo1', 'repo2', 'repo3']);
    }, 2000);
}