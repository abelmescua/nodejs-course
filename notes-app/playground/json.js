// var obj = {
// 	name: 'Andrew'
// };
// var stringObj = JSON.stringify(obj);
// console.log(typeof stringObj);
// console.log(stringObj);

// var personString = '{"name": "Andrew", "age": 25}';
// var person = JSON.parse(personString);
// console.log(typeof person);
// console.log(person);

// To create, write and read files
const fs = require('fs');

var originalNote = {
	title: 'Some title',
	body: 'Some body'
};
// originalNoteString
var originalNoteString = JSON.stringify(originalNote);

// creating a file and writting the value on it
fs.writeFileSync('notes.json', originalNoteString);

// reading value of that file
var noteString = fs.readFileSync('notes.json');
// note
var note = JSON.parse(noteString);
console.log(typeof note);
console.log(note.title);