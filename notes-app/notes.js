console.log('Starting notes.js');

// file system libraty to make files operations (create, write, remove)
const fs = require('fs');

var fetchNotes = () => {
  try {
    // taking the current notes file values
    var notesString = fs.readFileSync('notes-data.json');
    // putting them in our array of notes
    // this function turns string json to json object
    return JSON.parse(notesString);

  // This won't stop the program and everything will keep working
  } catch(e) {
    console.log('Error opening the file');
    return [];
  }
};

var saveNotes = (notes) => {
  // writing the new value into the file
  // this function turns json object to jsob string
  fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

// functions

// add a new note
var addNote = (title, body) => {
  var notes = fetchNotes();
  var note = {
  	title,
  	body
  };

// this return the value automatically
  var duplicateNotes = notes.filter((note) => note.title === title);

  if (duplicateNotes.length === 0) {
  	// Adding the new note
  	notes.push(note);
  	saveNotes(notes);
    return note;
  }
};

// get all notes
var getAll = () => {
  var notes = fetchNotes();
  debugger;
  return notes;
};

// get a specific note
var getNote = (title) => {
  debugger;
  // fetch the notes
  var notes = fetchNotes();
  debugger;
  // return the value
  var filteredNote = notes.filter((note) => note.title === title);

  debugger;
  // return the value
  return filteredNote[0];
};

// remove a note
var removeNote = (title) => {
  // fetch notes
  var notes = fetchNotes();

  // filter notes, removing the one with title or argument
  var filteredNotes = notes.filter((note) => note.title !== title);

  // save new notes array
  saveNotes(filteredNotes);

  return notes.length !== filteredNotes.length;
};

// print note data
var printNote = (note) => {
  // break on this line and user repl to output note
  // User read command with title
  debugger;
  console.log('--');
  console.log('Title: ', note.title);
  console.log('Body: ', note.body);
};

// exporting all functions
module.exports = {
  addNote,
  getAll,
  getNote,
  removeNote,
  printNote
};
