const yargs = require('yargs');
const axios = require('axios');

const key = 'AIzaSyBF9654NaAYVdM4PVgfCry9xP3dHnQNoCM';
const darkSkyKey = 'e2f47f289b4a1361b6fd985228f90e6c'

const argv = yargs
  .options({
    a: {
      demand: true,
      alias: 'address',
      describre: 'Address to fetch weather for',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

if (argv.address === '') { argv.address = '29602'; }

var encodedAddress = encodeURIComponent(argv.address);
var geocodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + encodedAddress + '&key=' + key;

// first api all with axios library
axios.get(geocodeUrl).then((response) => {
  if (response.data.status === 'ZERO_RESULTS') {
    // will trow this error in the terminal printed as a console.log()
    throw new Error('Unable to find that address.');
  }
  debugger;
  var lat = response.data.results[0].geometry.location.lat;
  var lng = response.data.results[0].geometry.location.lng;
  var weatherUrl = 'https://api.darksky.net/forecast/' + darkSkyKey + '/' + lat + ',' + lng;
  console.log(response.data.results[0].formatted_address);

  // second api call
  return axios.get(weatherUrl);

}).then((response) => {
  debugger;
  var temperature = response.data.currently.temperature;
  var apparentTemperature = response.data.currently.apparentTemperature;
  var dailySummary = response.data.daily.summary;
  var dailyData = response.data.daily.data;
  var hourlySummary = response.data.hourly.summary;

  console.log(`It's currently ${temperature}. It feels like ${apparentTemperature}`);
  console.log(`The daily summary is ${dailySummary}.`);
  console.log(`Morning ${dailyData[0].summary}. Afternoon ${dailyData[1].summary}. Evening ${dailyData[7].summary}`);
  console.log(`The hourly summary is ${hourlySummary}`);

}).catch((e) => {
  if (e.code === 'ENOTFOUND') {
    console.log('Unable to connect to API servers.');
  } else {
    console.log(e.message);
  }
});
