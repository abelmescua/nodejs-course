const request = require('request');
const key = 'AIzaSyBF9654NaAYVdM4PVgfCry9xP3dHnQNoCM';


/**
  function that gets an address and retuns something by a callback()
**/
var geocodeAddress = (address, callback) => {
  const encodedAddress = encodeURIComponent(address);

  // debugger;

  request({
    url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + encodedAddress + '&key=' + key,
    json: true
  }, (error, response, body) => {
    const status = body.status;
    // debugger;

    // Error handling
    if (error) {
      // first parameter is error as message
      callback('Unable to connect to the server');
    } else if (status === 'ZERO_RESULTS') {
      // first parameter is error as a message
      callback('Unable to find the address');
    } else if (status === 'OK') {
      const results = body.results[0];
      // first parameter is error as undefined and results as an object
      callback(undefined, {
        address: results.formatted_address,
        lat: results.geometry.location.lat,
        lng: results.geometry.location.lng
      });
    }
  });
}

module.exports = {
  geocodeAddress
};
