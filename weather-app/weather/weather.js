const request = require('request');

var getWeather = (lat, lng, callback) => {
  const darkSkyKey = 'e2f47f289b4a1361b6fd985228f90e6c'
  const darkSkyApi = 'https://api.darksky.net/forecast/' + darkSkyKey + '/' + lat + ',' + lng;
  debugger;

  request({
    url: darkSkyApi,
    json: true
  } ,(error, response, body) => {
    debugger;

    if (!error && response.statusCode === 200) {
      callback(undefined, body);
    } else {
      callback('Unable to fetch the weather');
    }

  });
};

module.exports = {
  getWeather
};
