const request = require('request');
const key = 'AIzaSyBF9654NaAYVdM4PVgfCry9xP3dHnQNoCM';

// Geocode address with promises
var geocodeAddress = (address) => {
  const encodedAddress = encodeURIComponent(address);

  debugger;

  // to use then() we always must return a Promise with (resolve, reject) functions
  return new Promise((resolve, reject) => {
    // once inside the promess function, we can use whatever request we want and return results with revolve or reject in case of success or errors
    request({
      url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + encodedAddress + '&key=' + key,
      json: true
    }, (error, response, body) => {
      const status = body.status;
      debugger;

      if (error) {
        // first parameter is error as message
        reject('Unable to connect to the server');
      } else if (status === 'ZERO_RESULTS') {
        // first parameter is error as a message
        reject('Unable to find the address');
      } else if (status === 'OK') {
        const results = body.results[0];
        resolve({
          address: results.formatted_address,
          lat: results.geometry.location.lat,
          lng: results.geometry.location.lng
        });
      }
    });
  });
};

// due to it returns a promise, we can use then().catch() functions
geocodeAddress('jajajajajja').then((location) => {
  console.log(JSON.stringify(location, undefined, 2));
}, (errorMessage) => {
  console.log(errorMessage);
})
