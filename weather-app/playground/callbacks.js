// callback function used as param to return a value
// the function get an id and returns a callback
var getUser = (id, callback) => {
  var user = {
    id: id,
    name: 'Vikram'
  };

  setTimeout(() => {
    // returning the user with callback function
    callback(user);
  }, 3000);
};

// calling the callback funtion, sending an id and expecting a user in return
getUser(31, (userObject) => {
  console.log(userObject);
});

/*
MORE EXAMPLES IN geocode.js FILE (used by app.js file)
*/
