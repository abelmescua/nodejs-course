var asyncAdd = (a, b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (typeof a === 'number' && typeof b === 'number') {
        resolve(a + b);
      } else {
        reject('Arguments must be numbers');
      }
    }, 1500);
  });
};

asyncAdd(2, 6).then((res) => {
  console.log('Result: ', res);
  return asyncAdd(res, 33
    // this then is for the second asyncAdd function calling inside of the first one
}).then((res) => {
  console.log('Should be 45', res);
  // we only need one catch to manage all the erros handeling
}).catch((errorMessage) => {
  console.log('Error: ', errorMessage);
});

// resolve and reject are callbacks funtions
// callbacks can be throw infinetly, but then a promess is trhow, it stops
// it can be resolve or reject once
// var somePromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     // resolve('Hey. It worked!');
//     reject('Unable to fulfill promise');
//   }, 2500);
// });
//
// somePromise.then((message) => {
//   console.log('Success: ', message);
// }, (errorMessage) => {
//   console.log('Error: ', errorMessage);
// });
