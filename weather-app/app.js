const yargs = require('yargs');

const geocode = require('./geocode/geocode.js');

const weather = require('./weather/weather.js');

const argv = yargs
  .options({
    a: {
      demand: true,
      alias: 'address',
      describre: 'Address to fetch weather for',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

// console.log(argv);

// debugger;

// first paramater=address to send
// second parameter is a callback expected that will return two parametes, but one of them will be undefined and the other not to know if was ok
geocode.geocodeAddress(argv.address, (errorMessage, results) => {
  debugger;

  if (errorMessage) {
    console.log(errorMessage);
  } else {
    console.log(JSON.stringify(results), undefined, 2);
    // weather api call
    weather.getWeather(results.lat, results.lng, (errorMessage, results) => {
      debugger;
      if (errorMessage) {
        console.log(errorMessage);
      } else {
        console.log(JSON.stringify(results));
      }
    });
  }
});
