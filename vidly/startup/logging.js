const winston = require('winston');
// require('winston-mongodb');
require('express-async-errors');

module.exports = function () {
  //  exceptions handler to logg the errors of promises(rejections) and exceptions
  winston.handleExceptions(
    new winston.transports.Console({ colorize: true, prettyPrint: true }),
    new winston.transports.File({ filename: 'uncaughtExceptions.log' })
    );

  // promises(rejections) handler
  process.on('unhandledRejection', (ex) => {
    // with this we handle the rejection as an exception in the upper function to be logged in a file
    throw ex;
  });

  // express routes handler (for API calls)
  winston.add(winston.transports.File, { filename: 'logfile.log' });
  // mongodb handler
  // winston.add(winston.transports.MongoDB, {
  //   db: 'mongodb://localhost/vidly',
  //   level: 'error'
  // });
}