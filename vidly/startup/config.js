const config = require('config');

module.exports = function() {
    if (!config.get('jwtPrivateKey')) {
        // this trhows an error and also stops the process, its like process.exit(1) +  console.log(error)
        // you can see more of this in /statup/logging.js module
        // 'export vidly_jwtPrivateKey=mySecureKey' to set the environment configuration var with some value
        throw new Error('FATAL ERROR: jwtPrivateKey is not defined.');
    }
}