const auth = require('../middleware/auth');
const { Rental, validate } = require('../models/rental');
const { Movie } = require('../models/movies');
const { Customer } = require('../models/customer');
const Fawn = require('fawn');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// transaction library init with our mongoose operations
Fawn.init(mongoose);

// routes
router.get('/', async (req, res) => {
    try {
        const rentals = await Rental.find().sort('-dateOut');
        res.send(rentals);
    } catch (err) {
        res.status(404).send({ error: err });
    }
});

router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send({ error: error.details[0].message });

    const customer = await Customer.findById(req.body.customerId);
    if (!customer) return res.status(400).send('Invalid customer.');

    const movie = await Movie.findById(req.body.movieId);
    if (!movie) return res.status(400).send('Invalid movie.');

    if (movie.numberInStock === 0) return res.status(400).send('Movie not available');

    const rental = new Rental({
        customer: {
            _id: customer._id,
            name: customer.name,
            phone: customer.phone
        },
        movie: {
            _id: movie._id,
            title: movie.title,
            dailyRentalRate: movie.dailyRentalRate
        }
    });
    try {
        // transaction operation
        new Fawn.Task()
            .save('rentals', rental)
            .update('movies', { _id: movie._id }, {
                $inc: { numberInStock: -1 }
            })
            .run();

        res.send(rental);
    } catch (ex) {
        res.status(500).send('Something failed.');
    }
});

router.put('/:id', async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send({ error: error.details[0].message });

    try {
        const result = await Rental.update({ _id: req.params.id }, {
            $set: {
                name: req.body.name
            }
        }, { new: true });
        res.send(result);
    } catch (err) {
        res.status(404).send({ error: err.message });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const result = await Rental.findByIdAndRemove({ _id: req.body.id });
        res.send(result);
    } catch (err) {
        res.status(404).send({ error: err.message });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const rental = await Rental.findById(req.params.id);
        res.send(rental);
    } catch (err) {
        res.status(404).send({ error: err.message });
    }
});

module.exports = router;