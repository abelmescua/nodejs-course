const auth = require('../middleware/auth');
const { Movie, validate } = require('../models/movies');
const { Genre } = require('../models/genres');
const express = require('express');
const router = express.Router();

// routes
router.get('/', async (req, res) => {
    try {
        const result = await Movie.find();
        res.send(result);
    } catch (err) {
        res.status(404).send(err.message);
    }
});

router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // checking that genre sent by cliet exists in DDBB
    const genre = await Genre.findById(req.body.genreId);
    if (!genre) return res.status(400).send({ error: 'Invalid genre.' });

    const movie = new Movie(req.body);
    try {
        await movie.save();
        res.send(movie);
    } catch (err) {
        res.status(400).send({ error: err.message });
    }
});

router.put('/:id', async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    try {
        const result = await Movie.update({ _id: req.params.id }, {
            $set: {
                title: req.body.title,
                numberInStock: req.body.numberInStock,
                dailyRentalRate: req.body.dailyRentalRate
            }
        }, { new: true });
        res.send(result);
    } catch (err) {
        res.status(400).send(err.message);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        console.log(req.params.id);
        const result = await Movie.findByIdAndRemove(req.params.id);
        res.send(result);
    } catch (err) {
        res.status(404).send({ error: err });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const movie = await Movie.findById(req.params.id);
        res.send(movie);
    } catch (err) {
        res.status(err.status).send(err.message);
    }
});

module.exports = router;