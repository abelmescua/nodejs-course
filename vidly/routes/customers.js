const auth = require('../middleware/auth');
const { Customer, validate } = require('../models/customer');
const express = require('express');
const router = express.Router();

// routes
router.get('/', async(req, res) => {
    try {
        const customers = await Customer.find()
        res.send(customers);
    } catch (err) {
        res.status(404).send({ error: error.message });
    }
});

router.post('/', auth, async(req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    try {
        const customer = new Customer({
            isGold: req.body.isGold,
            name: req.body.name,
            phone: req.body.phone
        });
        await customer.save();
        res.send(customer);
    } catch (err) {
        res.status(404).send({ error: err.message });
    }
});

router.put('/:id', async(req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    
    try {
        const value = {
            isGold: req.body.isGold,
            name: req.body.name,
            phone: req.body.phone
        };
        const result = await Customer.findByIdAndUpdate(req.params.id, value, { new: true });
        res.send(result);
    } catch (err) {
        res.status(404).send({ error: err.message });
    }
});

router.delete('/:id', async(req, res) => {
    try {
        const result = await Customer.findByIdAndRemove(req.params.id);
        res.send(result);
    } catch (err) {
        res.status(404).send({ error: err.message });
    }
});

router.get('/:id', async(req, res) => {
    try {
        const result = await Customer.findById(req.params.id);
        res.send(result);
    } catch (err) {
        res.status(404).send({ error: err.message });
    }
});

module.exports = router;