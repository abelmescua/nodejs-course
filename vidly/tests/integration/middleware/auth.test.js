const { User } = require('../../../models/user');
const { Genre } = require('../../../models/genres');
const request = require('supertest');

describe('auth middleware', () => {

    beforeEach(() => { server = require('../../../index'); });
    afterEach(async () => { 
        server.close();
        await Genre.remove({});
     });

    let token;

    const exec = function() {
        return request(server)
            .post('/api/genres')
            .set('x-auth-token', token)
            .send({ name: '12345' });
    }

    beforeEach(() => {
        token = new User().generateAuthToken();
    });

    it('should return 401 if no token is provided', async () => {
        token = '';

        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 400 if no token is invalid', async () => {
        token = null;

        const res = await exec();

        expect(res.status).toBe(400);
    });

    it('should return 200 if no token is valid', async () => {
        const res = await exec();

        expect(res.status).toBe(200);
    });
});