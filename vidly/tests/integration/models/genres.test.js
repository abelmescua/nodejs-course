const request = require('supertest');
const { Genre } = require('../../../models/genres');
const { User } = require('../../../models/user');
const mongoose = require('mongoose');

let server;

describe('/api/genres', () => {
    beforeEach(() => { server = require('../../../index'); });
    afterEach(async () => {
        // closing server and clean up db after each test
        server.close();
        await Genre.remove({});
    });

    describe('GET /', () => {
        it('should return all genres', async () => {
            // inserting items in DB
            await Genre.collection.insertMany([
                { name: 'genre1' },
                { name: 'genre2' }
            ]);

            // calling
            const res = await request(server).get('/api/genres');

            // checking that we get them
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some(g => g.name === 'genre1')).toBeTruthy();
            expect(res.body.some(g => g.name === 'genre2')).toBeTruthy();
        });
    });

    describe('GET /:id', () => {
        it('should return a genre if valid id is passed', async () => {
            const payload = { _id: mongoose.Types.ObjectId().toHexString(), name: 'Action' };
            const genre = new Genre(payload);
            await genre.save();

            const res = await request(server).get('/api/genres/' + genre._id);

            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name', genre.name);
        });

        it('should return 404 if invalid id is passed', async () => {
            const res = await request(server).get('/api/genres/1');

            expect(res.status).toBe(404);
        });

        it('should return 404 if not genre with the given id exists', async () => {
            const id = mongoose.Types.ObjectId().toHexString();
            const res = await request(server).get('/api/genres/' + id);

            expect(res.status).toBe(404);
        });
    });

    describe('POST /', () => {

        let token;
        let name;

        const exec = async function () {
            return await request(server)
                .post('/api/genres')
                .set('x-auth-token', token)
                .send({ name });
        }

        beforeEach(() => {
            token = new User().generateAuthToken();
            name = '12345';
        });

        it('should return 401 if client is not logged in', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 400 if genres name less than 5 characters', async () => {
            name = '1234';
            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 400 if genres name more than 50 characters', async () => {
            name = new Array(52).join('a');

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 200 if genres is correct', async () => {
            const res = await exec();

            const genre = await Genre.find({ name: '12345' });

            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', '12345');
            expect(genre).not.toBeNull();
        });
    });

    describe('PUT /:id', () => {
        it('should return 404 if not valid ID is passed', async () => {
            const res = await request(server).put('/api/genre/1');

            expect(res.status).toBe(404);
        });

        it('should return 400 if not valid genre is passed', async () => {
            const genre = new Genre({ _id: mongoose.Types.ObjectId().toHexString(), name: '12' });

            const res = await request(server)
                .put('/api/genres/' + genre._id)
                .send({ name: genre.name });

            expect(res.status).toBe(400);
        });

        it('should return 200 and update if valid genre is passed', async () => {
            const genre = new Genre({ _id: mongoose.Types.ObjectId().toHexString(), name: '12345' });
            await genre.save();

            const res = await request(server)
                .put('/api/genres/' + genre._id)
                .send({ name: 'newName' });

            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name', 'newName');
        });
    });

    describe('DELETE /:id', () => {
       
        it('should return 401 if token is not provided', async () => {
            const token = '';
            const id = mongoose.Types.ObjectId().toHexString();

            const res = await request(server)
                .delete('/api/genres/' + id)
                .set('x-auth-token', token);

            expect(res.status).toBe(401);
        });

        it('should return 400 if not valid token is provided', async () => {
            const token = '12345';
            const id = mongoose.Types.ObjectId().toHexString();

            const res = await request(server)
                .delete('/api/genres/' + id)
                .set('x-auth-token', token);

            expect(res.status).toBe(400);
        });

        it('should return 403 if user is not admin', async () => {
            const token = new User({ isAdmin: false }).generateAuthToken();
            const id = mongoose.Types.ObjectId().toHexString();

            const res = await request(server)
                .delete('/api/genres/' + id)
                .set('x-auth-token', token);

            expect(res.status).toBe(403);
        });

        it('should return 404 if user is not admin', async () => {
            const token = new User({ isAdmin: false }).generateAuthToken();
            const id = mongoose.Types.ObjectId().toHexString();

            const res = await request(server)
                .delete('/api/genres/' + id)
                .set('x-auth-token', token);

            expect(res.status).toBe(404);
        });
    });
})