const mongoose = require('mongoose');
const Joi = require('joi');

// schema
const customersSchema = mongoose.Schema({
    isGold: {
        type: Boolean,
        require: true,
        default: false
    },
    name: {
        type: String,
        minlength: 5,
        maxlength: 50,
        require: true
    },
    phone: {
        type: String,
        minlength: 5,
        maxlength: 50,
        require: true
    }
});

// model
const Customer = mongoose.model('customers', customersSchema);

function validateCustomer(customer) {
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        phone: Joi.string().min(5).max(50).required(),
        isGold: Joi.boolean().required()
    }

    return Joi.validate(customer, schema);
}

exports.Customer = Customer;
exports.validate = validateCustomer;