const mongoose = require('mongoose');
const Joi = require('joi');

// model
const genreSchema = mongoose.Schema({
  name: {
    type: String,
    min: 3,
    max: 10,
    // enum: ['Action', 'Horror', 'Romance'],
    required: true
  }
});

function validateGenre(genre) {
    const schema = {
      name: Joi.string().min(5).max(50).required()
    };
  
    return Joi.validate(genre, schema);
  }

const Genre = mongoose.model('genres', genreSchema);

exports.genreSchema = genreSchema;
exports.Genre = Genre;
exports.validate = validateGenre;