const Joi = require('joi');
const PasswordComplexity = require('joi-password-complexity');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('config');

// schema
const userSchema = mongoose.Schema({
    name: {
        type: String,
        minlength: 4,
        maxlength: 255,
        required: true
    },
    email: {
        type: String,
        minlength: 4,
        maxlength: 255,
        required: true,
        unique: true
    },
    password: {
        type: String,
        minlength: 4,
        maxlength: 255,
        required: true
    },
    isAdmin: Boolean
});

// methods
userSchema.methods.generateAuthToken = function() {
    const token = jwt.sign({ _id: this._id, isAdmin: this.isAdmin }, config.get('jwtPrivateKey'));
    return token;
};

// model
const User = mongoose.model('users', userSchema);

// validation
function validateUser(user) {
    const schema = {
        name: Joi.string().min(5).max(255).required(),
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()
    };

    return Joi.validate(user, schema);
}

async function validatePassoword(password) {
    console.log(password)
    Joi.validate(password, new PasswordComplexity(), (error, value) => {
        console.log(error.details[0].message);
        console.log(value)

    });
}

exports.User = User;
exports.validate = validateUser;
exports.validatePassoword = validatePassoword;