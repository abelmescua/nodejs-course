const Joi = require('joi');
const mongoose = require('mongoose');
const { genreSchema } = require('./genres');

// model with schema
const Movie = mongoose.model('movies', new mongoose.Schema({
    title: {
        type: String,
        minlength: 5,
        maxlength: 30,
        trim: true,
        required: true
    },
    genre: {
        type: genreSchema,
        required: true
    },
    numberInStock: {
        type: Number,
        required: true
    },
    dailyRentalRate: {
        type: Number,
        required: true
    }
}));

function validateMovie(movie) {
    const schema = {
        title: Joi.string().min(5).max(30).required(),
        genreId: Joi.objectId().required(),
        numberInStock: Joi.number().required(),
        dailyRentalRate: Joi.number().required()
    };

    return Joi.validate(movie, schema);
}

exports.validate = validateMovie;
exports.Movie = Movie;