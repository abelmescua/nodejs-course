


async function dispatchEmail() {
  try {
    const customer = await getCustomer(1);
    console.log('Customer', customer);
    if (customer.isGold) {
      const movies = await getTopMovies();
      const email = await sendEmail(customer.email, movies);
      console.log(email);
    }
  } catch (err) {
    console.log('Error', err.message);
  }
}

dispatchEmail();

function getCustomer(id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        id: id,
        name: 'Mosh Hamedani',
        isGold: true,
        email: 'email'
      });
    }, 4000);
  })
}

function getTopMovies() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(['movie1', 'movie2']);
    }, 4000);
  });
}

function sendEmail(email, movies) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Email sent...');
    }, 4000);
  });
}